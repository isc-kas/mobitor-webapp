package ch.mobi.mobitor.domain.config;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.plugin.liima.config.AppServerConfig;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class AppServerConfigTestFactory {

    public static Map<String, AppServerConfig> createMinimalAppServerConfigMap() {
        Map<String, AppServerConfig> appSerNameToConfigMap = new HashMap<>();
        AppServerConfig appServConf1 = new AppServerConfig();
        appServConf1.setApplicationNames(Collections.singletonList("app1"));
        appSerNameToConfigMap.put("appServ1", appServConf1);

        AppServerConfig appServConf2 = new AppServerConfig();
        appServConf2.setApplicationNames(Collections.singletonList("app2"));
        appSerNameToConfigMap.put("appServ2", appServConf2);

        return appSerNameToConfigMap;
    }
}
