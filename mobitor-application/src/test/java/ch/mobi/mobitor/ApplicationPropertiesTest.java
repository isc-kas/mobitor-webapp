package ch.mobi.mobitor;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import org.junit.jupiter.api.Test;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.util.Properties;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class ApplicationPropertiesTest {

    @Test
    public void testApplicationPropertiesHaveSameConfigs() throws IOException {
        PathMatchingResourcePatternResolver pathResolver = new PathMatchingResourcePatternResolver();
        Resource[] resources = pathResolver.getResources("classpath*:application-*.properties");

        Set<String> referencePropNames = null;
        int referencePropSize = 0;

        for (Resource resource : resources) {
            System.out.println(resource.getFilename());
            Properties props = new Properties();
            props.load(resource.getInputStream());

            Set<String> propNames = props.stringPropertyNames();
            if (referencePropNames == null) {
                referencePropNames = propNames;
                referencePropSize = propNames.size();
            } else {
                assertThat(propNames).containsAll(referencePropNames);
                assertThat(propNames.size()).isEqualTo(referencePropSize);
            }
        }
    }
}
