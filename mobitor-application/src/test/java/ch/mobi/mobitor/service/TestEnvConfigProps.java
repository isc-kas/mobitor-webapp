package ch.mobi.mobitor.service;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2018 - 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.ConfigDomainMapping;
import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.config.EnvironmentPipeline;

import java.util.List;

public class TestEnvConfigProps implements EnvironmentConfigProperties {

        private final String env;

        public TestEnvConfigProps(String env) {
            this.env = env;
        }

        @Override
        public String getEnvironment() {
            return env;
        }

        @Override
        public boolean isBuildEnvironment() {
            return false;
        }

        @Override
        public String getName() {
            return null;
        }

        @Override
        public String getLabel() {
            return "Test Env Label";
        }

        @Override
        public String getDescription() {
            return null;
        }

        @Override
        public String getDescriptionUrl() {
            return null;
        }

        @Override
        public EnvironmentNetwork getNetwork() {
            return null;
        }

        @Override
        public boolean isReachableFromLocalhost() {
            return false;
        }

        @Override
        public EnvironmentPipeline getPipeline() {
            return null;
        }

        @Override
        public String getTimestampUrl() {
            return null;
        }

        @Override
        public List<ConfigDomainMapping> getConfigDomainMappings() {
            return null;
        }
    }
