package ch.mobi.mobitor.service;

/*-
 * §
 * mobitor-application
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.config.EnvironmentPipeline;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO not too sure why this is needed, the rest plugin contains a default implementation
@Service
@Profile("!test,!allpluginsenabledtest")
public class DefaultEnvironmentConfigurationService implements EnvironmentsConfigurationService {
    @Override
    public boolean isNetworkReachable(String environment) {
        return true;
    }

    @Override
    public boolean isNetworkReachable(EnvironmentNetwork network) {
        return true;
    }

    @Override
    public EnvironmentNetwork getCurrentNetwork() {
        return EnvironmentNetwork.DEVELOPMENT;
    }

    @Override
    public String getName(String environment) {
        return null;
    }

    @Override
    public EnvironmentPipeline getPipeline(String environment) {
        return null;
    }

    @Override
    public EnvironmentConfigProperties getEnvironmentConfig(String environment) {
        return null;
    }

    @Override
    public List<EnvironmentConfigProperties> getEnvironments() {
        return null;
    }

    @Override
    public boolean isEnvironmentReachable(String environment) {
        return false;
    }

    @Override
    public String getStage(String environment) {
        return "STAGE";
    }

}
